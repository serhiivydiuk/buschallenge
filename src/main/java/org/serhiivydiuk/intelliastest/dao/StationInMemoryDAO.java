package org.serhiivydiuk.intelliastest.dao;

import org.serhiivydiuk.intelliastest.model.Station;
import org.serhiivydiuk.intelliastest.store.StationsInMemoryStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by serhii on 05.07.17.
 */
@Repository
public class StationInMemoryDAO implements StationDAO {

    @Autowired
    private StationsInMemoryStore stationsInMemoryStore;

    @Override
    public Station getStationById(int id) {
        return stationsInMemoryStore.getById(id);
    }

}
