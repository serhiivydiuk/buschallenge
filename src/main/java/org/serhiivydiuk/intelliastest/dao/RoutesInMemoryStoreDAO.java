package org.serhiivydiuk.intelliastest.dao;

import org.serhiivydiuk.intelliastest.model.Route;
import org.serhiivydiuk.intelliastest.model.Station;
import org.serhiivydiuk.intelliastest.store.RoutesInMemoryStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by serhii on 05.07.17.
 */
@Repository
public class RoutesInMemoryStoreDAO implements RoutesDAO {

    @Autowired
    private RoutesInMemoryStore routesInMemoryStore;

    @Override
    public List<Route> getRoutesByStation(Station station) {
        List<Route> routes = new ArrayList<>(station.getRoutesIds().size());
        station.getRoutesIds().forEach(inst -> routes.add(routesInMemoryStore.getRouteById(inst)));
        return routes;
    }

}
