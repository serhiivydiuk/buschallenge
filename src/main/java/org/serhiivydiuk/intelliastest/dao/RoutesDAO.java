package org.serhiivydiuk.intelliastest.dao;

import org.serhiivydiuk.intelliastest.model.Route;
import org.serhiivydiuk.intelliastest.model.Station;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by serhii on 05.07.17.
 */
@Repository
public interface RoutesDAO {

    public List<Route> getRoutesByStation(Station station);

}
