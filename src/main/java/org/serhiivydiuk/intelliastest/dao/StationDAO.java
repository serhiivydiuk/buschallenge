package org.serhiivydiuk.intelliastest.dao;

import org.serhiivydiuk.intelliastest.model.Station;
import org.springframework.stereotype.Repository;

/**
 * Created by serhii on 05.07.17.
 */
@Repository
public interface StationDAO {

    public Station getStationById(int id);

}
