package org.serhiivydiuk.intelliastest.store;

import org.serhiivydiuk.intelliastest.model.Route;
import org.springframework.stereotype.Component;

/**
 * Created by serhii on 05.07.17.
 */
@Component
public interface RoutesInMemoryStore {

    public Route getRouteById(int routeId);

    public void addRoute(Route route);

}
