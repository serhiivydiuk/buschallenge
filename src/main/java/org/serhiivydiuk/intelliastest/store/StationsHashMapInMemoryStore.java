package org.serhiivydiuk.intelliastest.store;

import org.serhiivydiuk.intelliastest.model.Route;
import org.serhiivydiuk.intelliastest.model.Station;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by serhii on 05.07.17.
 */
@Component
public class StationsHashMapInMemoryStore implements StationsInMemoryStore {

    private Map<Integer, Station> stationStore; // <stationId, Station>

    public StationsHashMapInMemoryStore() {
        stationStore = new HashMap<>();
    }

    @Override
    public Station getById(int id) {
        return stationStore.get(id);
    }

    @Override
    public void add(Station station) {
        if(stationStore.containsKey(station.getStationId())) {
            Station oldStation = stationStore.get(station.getStationId());
            updateStationRoutes(oldStation, station);
        } else {
            stationStore.put(station.getStationId(), station);
        }
    }

    private void updateStationRoutes(Station oldStation, Station station){
        List<Integer> oldRoutesIds = oldStation.getRoutesIds();
        oldRoutesIds.addAll(station.getRoutesIds());
    }

}
