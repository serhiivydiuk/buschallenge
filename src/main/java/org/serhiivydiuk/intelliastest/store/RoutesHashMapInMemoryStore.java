package org.serhiivydiuk.intelliastest.store;

import org.serhiivydiuk.intelliastest.model.Route;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by serhii on 05.07.17.
 */
@Component
public class RoutesHashMapInMemoryStore implements RoutesInMemoryStore {

    private Map<Integer, Route> routesStore; // <routeId, Route>

    public RoutesHashMapInMemoryStore() {
        routesStore = new HashMap<Integer, Route>();
    }

    @Override
    public Route getRouteById(int routeId) {
        return routesStore.get(routeId);
    }

    @Override
    public void addRoute(Route route) {
        routesStore.put(route.getRouteId(), route);
    }

}
