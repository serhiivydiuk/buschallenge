package org.serhiivydiuk.intelliastest.store;

import org.serhiivydiuk.intelliastest.model.Station;
import org.springframework.stereotype.Component;

/**
 * Created by serhii on 05.07.17.
 */
@Component
public interface StationsInMemoryStore {

    public Station getById(int id);

    public void add(Station station);

}
