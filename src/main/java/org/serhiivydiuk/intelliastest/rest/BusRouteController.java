package org.serhiivydiuk.intelliastest.rest;

import org.serhiivydiuk.intelliastest.exchange.StationsDirectRouteResponse;
import org.serhiivydiuk.intelliastest.service.BusRouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by serhii on 05.07.17.
 */

@RestController
@RequestMapping("/api")
public class BusRouteController {

    @Autowired
    private BusRouteService busRouteService;

    @RequestMapping(value = "/direct", method = RequestMethod.GET)
    public ResponseEntity<StationsDirectRouteResponse> isRouteExist(@RequestParam("dep_sid") int departureSid, @RequestParam("arr_sid") int arrivalSid) {
        boolean directBusRoute = busRouteService.isDirectBusRoute(departureSid, arrivalSid);
        StationsDirectRouteResponse response = new StationsDirectRouteResponse(departureSid, arrivalSid, directBusRoute);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
