package org.serhiivydiuk.intelliastest.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by serhii on 05.07.17.
 */
public class Station {

    private int stationId;
    private List<Integer> routesIds = new ArrayList<>(5);

    public Station(int stationId) {
        this.stationId = stationId;
    }

    public int getStationId() {
        return stationId;
    }

    public void setStationId(int stationId) {
        this.stationId = stationId;
    }

    public List<Integer> getRoutesIds() {
        return routesIds;
    }

    public void setRoutesIds(List<Integer> routesIds) {
        this.routesIds = routesIds;
    }
}
