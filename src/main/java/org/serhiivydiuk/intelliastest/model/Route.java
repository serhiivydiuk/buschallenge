package org.serhiivydiuk.intelliastest.model;

import java.util.List;

/**
 * Created by serhii on 05.07.17.
 */
public class Route {

    private int routeId;
    private List<Station> stations;

    public Route(int routeId) {
        this.routeId = routeId;
    }

    public int getRouteId() {
        return routeId;
    }

    public void setRouteId(int routeId) {
        this.routeId = routeId;
    }

    public List<Station> getStations() {
        return stations;
    }

    public void setStations(List<Station> stations) {
        this.stations = stations;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj != null && obj instanceof Route) {
            Route that = (Route) obj;
            if (this.routeId == that.routeId) {
                return true;
            }
        }
        return false;
    }
}
