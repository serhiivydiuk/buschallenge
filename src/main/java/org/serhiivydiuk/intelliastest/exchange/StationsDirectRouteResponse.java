package org.serhiivydiuk.intelliastest.exchange;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Created by serhii on 05.07.17.
 */
public class StationsDirectRouteResponse implements Serializable {
    @JsonProperty("dep_sid")
    private int depSid;
    @JsonProperty("arr_sid")
    private int arrSid;
    @JsonProperty("direct_bus_route")
    private boolean directBusRoute;

    public StationsDirectRouteResponse() {
    }

    public StationsDirectRouteResponse(int depSid, int arrSid, boolean directBusRoute) {
        this.depSid = depSid;
        this.arrSid = arrSid;
        this.directBusRoute = directBusRoute;
    }

    public int getDepSid() {
        return depSid;
    }

    public void setDepSid(int depSid) {
        this.depSid = depSid;
    }

    public int getArrSid() {
        return arrSid;
    }

    public void setArrSid(int arrSid) {
        this.arrSid = arrSid;
    }

    public boolean isDirectBusRoute() {
        return directBusRoute;
    }

    public void setDirectBusRoute(boolean directBusRoute) {
        this.directBusRoute = directBusRoute;
    }
}
