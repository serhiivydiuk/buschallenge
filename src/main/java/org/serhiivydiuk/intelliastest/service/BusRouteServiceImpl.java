package org.serhiivydiuk.intelliastest.service;

import org.serhiivydiuk.intelliastest.dao.RoutesDAO;
import org.serhiivydiuk.intelliastest.dao.StationDAO;
import org.serhiivydiuk.intelliastest.model.Route;
import org.serhiivydiuk.intelliastest.model.Station;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by serhii on 05.07.17.
 */
@Service
public class BusRouteServiceImpl implements BusRouteService {

    @Autowired
    private RoutesDAO routesDAO;

    @Autowired
    private StationDAO stationDAO;

    Logger logger = LoggerFactory.getLogger(BusRouteServiceImpl.class);

    @Override
    public boolean isDirectBusRoute(int departureStationId, int arrivalStationId) {
        Station departureStation = stationDAO.getStationById(departureStationId);
        Station arrivalStation = stationDAO.getStationById(arrivalStationId);
        if (arrivalStation == null || departureStation == null) {
            logger.info("There is not any route between stations " + departureStationId + " and " + arrivalStationId + "" +
                    " Cause: One of them does not exist in routes.txt");
            return false;
        }
        List<Route> routesByDepartureStation = routesDAO.getRoutesByStation(departureStation);
        List<Route> routesByArrivalStation = routesDAO.getRoutesByStation(arrivalStation);
        return checkDirectRoute(routesByDepartureStation, routesByArrivalStation);
    }


    private boolean checkDirectRoute(List<Route> routesByDepartureStation, List<Route> routesByArrivalStation) {
        for (Route route : routesByArrivalStation) {
            if (routesByDepartureStation.contains(route)) {
                return true;
            }
        }
        return false;
    }
}
