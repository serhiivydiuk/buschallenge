package org.serhiivydiuk.intelliastest.service;


import org.springframework.stereotype.Service;

/**
 * Created by serhii on 05.07.17.
 */

@Service
public interface BusRouteService {

    boolean isDirectBusRoute(int departureStationId, int arrivalStationId);

}
