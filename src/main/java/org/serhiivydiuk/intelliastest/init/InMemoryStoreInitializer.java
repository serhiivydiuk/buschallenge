package org.serhiivydiuk.intelliastest.init;

import org.serhiivydiuk.intelliastest.model.Route;
import org.serhiivydiuk.intelliastest.model.Station;
import org.serhiivydiuk.intelliastest.store.RoutesInMemoryStore;
import org.serhiivydiuk.intelliastest.store.StationsInMemoryStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by serhii on 05.07.17.
 */
@Component
public class InMemoryStoreInitializer implements StoreInitializer {

    private final static String FILE_PATH = "data/routes.txt";
    private Logger logger = LoggerFactory.getLogger(InMemoryStoreInitializer.class);

    @Autowired
    private RoutesInMemoryStore routesInMemoryStore;

    @Autowired
    private StationsInMemoryStore stationsInMemoryStore;

    @PostConstruct
    private void loadRoutesFromFile() {
        logger.info("Loading routes from file... ");
        try {
            Path path = Paths.get(FILE_PATH);
            Files.lines(path).forEach(route -> {
                String[] stations = route.split(" ");
                if (route.length() > 2) {
                    saveToMemory(stations);
                }
            });
        } catch (IOException e) {
            logger.error("Cannot load routes. ", e);
        }
    }

    private void saveToMemory(String[] stations) {
        List<Station> routeStationsList = new ArrayList<>(stations.length);
        int routeId = Integer.valueOf(stations[0]);  // get route id
        for (int i = 1; i < stations.length; i++) { // save stations to collection
            Station station = new Station(Integer.valueOf(stations[i]));
            station.getRoutesIds().add(routeId);
            routeStationsList.add(station);
            saveStation(station);
        }
        saveRoute(routeId, routeStationsList);
    }

    private void saveStation(Station station) {
        stationsInMemoryStore.add(station);
    }

    private void saveRoute(int id, List<Station> routeStations) {
        Route route = new Route(id);
        route.setStations(routeStations);
        routesInMemoryStore.addRoute(route);
    }
}
