package org.serhiivydiuk.intelliastest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * Created by serhii on 05.07.17.
 */

@SpringBootApplication
@EnableWebMvc
@EnableAutoConfiguration
public class BusRouteApp {

    public static void main(String[] args) {
        SpringApplication.run(BusRouteApp.class, args);
    }

}
